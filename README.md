# Start Codon Analysis

get_bacteria_add_genus.py 
- input: 'organism_names.txt'
- output: 'bacteria_names_with_genus.txt'

figure_one.py
- input: 'bacteria_names_with_genus.txt'
- output: displays AUG frequency per phylum (fig. 1)

generate_plot.py
- input: 'bacteria_names_with_genus.txt'
- output: displays start codon frequencies per gene/genus pair (fig. 2)

get_genus_occurrences.py
- input: 'bacteria_names_with_genus.txt'
- output: 'phylum_genus_occurrence_counts.txt' (determined the 30 genuses used)