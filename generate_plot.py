#!/usr/bin/env python
# coding: utf-8

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

df_csv = pd.read_csv("bacteria_names_with_genus.txt", sep="\t", dtype={"partial": str, "GCA_Number": str})
df = df_csv

cutoff = 10                 #number of genuses in which nonAUG ratio > 0.5
cutoffpresence = 20         #number of genuses in which the gene is present
genusfiltertype = 'all30phyla'
genuses = [#betaproteobacteria
            "Bordetella", 
            "Neisseria", #betaproteobacteria added
            #gammaproteobacteria
            "Mannheimia", 
            "Pasteurella",
            "Serratia", 
            "Salmonella",   
            "Klebsiella",
            "Escherichia", 
            "Enterobacter", 
            "Citrobacter", 
            "Xanthomonas", 
            "Pseudomonas", 
            #delta/epsilon
            "Helicobacter", 
            "Campylobacter",
            #firmicutes
            "Clostridioides", #firmicute added
            "Clostridium", #firmicute added
            "Lactobacillus",
            "Enterococcus",  #firmicute added
            "Streptococcus",
            "Brevibacillus", #firmicute added
            "Paenibacillus", #firmicute added
            "Listeria", 
            "Bacillus",
            "Staphylococcus",
            #actinobacteria
            "Streptomyces", 
            "Mycobacterium", 
            "Corynebacterium",
            "Bifidobacterium",
            #tenericutes
            "Mycoplasma", #tenericute added
            #planctomycetes
            "Planctomycetes"
            ]

df = df[df.genus.isin(genuses)]

aggfunction = "count"

# calculates number of occurrences per start codon for a gene
df_sc = df.groupby(["gene", "start_codon", "domain", "kingdom", "phylum", "genus"]).agg({"file_name":[aggfunction]})
df_sc.columns = df_sc.columns.droplevel(0)
df_sc.reset_index(inplace=True)
df_sc.rename(columns={aggfunction: "sc_" + aggfunction}, inplace=True)

# calculates number of occurrences for all start codons for a gene
df_all = df.groupby(["gene", "domain", "kingdom", "phylum", "genus"]).agg({"file_name":[aggfunction]})
df_all.columns = df_all.columns.droplevel(0)
df_all.reset_index(inplace=True)
df_all.rename(columns={aggfunction: "all_" + aggfunction}, inplace=True)

df_join = df_sc.merge(df_all, on=["gene", "domain", "kingdom", "phylum", "genus"])

# calculates occurrences per start codon / total occurrences
df_join["ratio"] = df_join["sc_count"] / df_join["all_count"]

# FINDS TOP 25 GENES
# calculates number of occurrences for all nonATG start codons for a gene
df_nonatg = df_join[df_join.start_codon != "ATG"]
df_nonatg = df_nonatg.groupby(["gene", "domain", "kingdom", "phylum", "genus"]).agg({"sc_count":["sum"]})
df_nonatg.columns = df_nonatg.columns.droplevel(0)
df_nonatg.reset_index(inplace=True)
df_nonatg.rename(columns={"sum": "nonatg_count"}, inplace=True)

# calculates nonATG occurrences / total occurrences
df_all_1 = df_join.merge(df_nonatg, on=["gene", "domain", "kingdom", "phylum", "genus"], how="left")
df_all_1 = df_all_1.fillna(0)
df_all_1["nonatg_ratio"] = df_all_1["nonatg_count"] / df_all_1["all_count"]
df_all_including_atg = df_all_1

#cutoff of presence in all genuses
df_all_2 = df_all_1.groupby("gene").agg({"genus":["nunique"]})
df_all_2.columns = df_all_2.columns.droplevel(0)
df_all_2.reset_index(inplace=True)
df_all_2 = df_all_2[df_all_2["nunique"] >= cutoffpresence]

genespresent = pd.unique(df_all_2["gene"])

df_all_1 = df_all_1[df_all_1["gene"].isin(genespresent)]
df_all_1 = df_all_1[(df_all_1.nonatg_ratio > 0.5)]

df_genuscount = df_all_1.groupby("gene").agg({"genus":["nunique"]})
df_genuscount.columns = df_genuscount.columns.droplevel(0)
df_genuscount.reset_index(inplace=True)

df_genuscount = df_genuscount[df_genuscount["nunique"] >= cutoff]
df_genuscount = df_genuscount.sort_values(['nunique'], ascending=False)
#print(df_genuscount)
geneslist = list(df_genuscount.gene.unique())
print(geneslist)


# calculate the ratios for gene-genus-startcodon combinations
df_top_genes = df_all_including_atg[df_all_including_atg["gene"].isin(geneslist)]

t = pd.pivot_table(df_top_genes, values=["ratio"], index=["genus", "gene"], columns=["start_codon"], aggfunc=max, fill_value=0, dropna=False)
t.columns = t.columns.droplevel(0)
t.reset_index(inplace=True)

t.to_csv('table_percentages_equilibrized_genes_cutoff' + str(cutoff) + '_' + genusfiltertype + '.txt', sep='\t')
print("Percentages generated.")

df_csv = pd.read_csv('table_percentages_equilibrized_genes_cutoff' + str(cutoff) + '_' + genusfiltertype + '.txt', sep='\t')

plt.figure(figsize=(0.13+0.195*len(geneslist), 6), dpi=200)

total_rows = len(genuses)
total_cols = len(geneslist)
row_count = total_rows
col_count = total_cols

# https://matplotlib.org/3.2.1/gallery/color/named_colors.html
colorlist = {'ATA':'tab:blue', 'ATC':'tab:purple', 'ATG':'tab:green', 
             'ATT':'tab:red', 'CCC':'mediumpurple', 'CTG':'tab:brown', 
             'GCA':'rebeccapurple', 'GTA':'lavenderblush', 'GTG':'tab:olive',
             'TAT':'aquamarine', 'TGT':'peru', 'TTG':'tab:orange',
             'CCG':'salmon', 'CAA':'fuchsia', 'CTA':'crimson',
             'AAG':'peachpuff', 'ACC':'beige', 'ACT':'darkslategray',
             'CAC':'lightcyan', 'CAG':'antiquewhite', 'CTC':'seagreen',
             'GAA':'whitesmoke', 'GCG':'floralwhite', 'GGT':'cornflowerblue',
             'GTT':'indigo', 'TCC':'firebrick', 'TCT':'tomato'}

for row in range(row_count):
    genus = genuses[row]
    print(row, genus)
    df_genus = df_csv[df_csv["genus"] == genus]
    for col in range(col_count):
        gene = geneslist[col]
        df_genus_gene = df_genus[df_genus["gene"] == gene]
        i = (row * col_count) + col
        #print(row, col, i, genus, gene)
        plt.subplot(row_count, col_count, i+1)
        plt.box(on=False)
        if i >= col_count*(row_count-1):
            capitalgene = ""
            if len(gene) == 4:
                capitalgene = gene[:3] + gene[3:].capitalize()
            else: 
                capitalgene = gene            
            plt.xlabel(capitalgene, rotation=90)
        plt.xticks([])
        if i % col_count == 0:
            plt.ylabel(genus, rotation=0, labelpad=5, 
                       va="center", fontsize=10, ha="right")
        plt.yticks([])
        one_row = df_genus_gene.iloc[0, :].values
        #print(one_row)
        bottom_edge = 0
        col_index = 3
        for start_codon_percentage in one_row[3:]:
            start_codon = df_csv.columns[col_index]
            color_value = colorlist[start_codon]
            plt.bar(0, start_codon_percentage, bottom=bottom_edge, color=color_value)
            bottom_edge += start_codon_percentage
            col_index += 1
            
plt.show()

