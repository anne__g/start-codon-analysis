# -*- coding: utf-8 -*-

import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

# %% filter phyla
df_csv = pd.read_csv("bacteria_names_with_genus.txt", sep="\t", dtype={"partial": str, "GCA_Number": str})
df = df_csv

phylofilter = ["Fusobacteriia", "Verrucomicrobia", "Cyanobacteria", "Tenericutes", "Planctomycetes", "Bacteroidetes/Chlorobi group", 
               "Alphaproteobacteria", "delta/epsilon subdivisions", "Actinobacteria", "Betaproteobacteria", "Firmicutes", "Gammaproteobacteria"]
df = df[df.phylum.isin(phylofilter)]

aggfunction = "count"

# %% calculate gene-start codon counts per phylum
df_sc = df.groupby(["start_codon", "domain", "kingdom", "phylum"]).agg({"file_name":[aggfunction]})
df_sc.columns = df_sc.columns.droplevel(0)
df_sc.reset_index(inplace=True)
df_sc.rename(columns={aggfunction: "sc_" + aggfunction}, inplace=True)

# %% calculate total number of gene counts per phylum
df_gene = df.groupby(["domain", "kingdom", "phylum"]).agg({"file_name":[aggfunction]})
df_gene.columns = df_gene.columns.droplevel(0)
df_gene.reset_index(inplace=True)
df_gene.rename(columns={aggfunction: "all_" + aggfunction}, inplace=True)

# %% join dataframes
df_join = df_sc.merge(df_gene, on=["domain", "kingdom", "phylum"])

# %% calculate percentage of ATG occurrence
df_join["ratio"] = df_join["sc_count"] / df_join["all_count"]
scfilter = ["ATG"]
df_join = df_join[df_join.start_codon.isin(scfilter)]
df_sort = df_join.sort_values(["ratio"], ascending=True)
#print(df_join)

# %% create graph
ratios = df_sort["ratio"]
#ratios.plot.bar()

x = np.arange(len(ratios))
plt.figure(figsize=(12, 6), dpi = 200)
plt.bar(x, ratios)
plt.xticks(x, list(df_sort["phylum"]), rotation = 90)
