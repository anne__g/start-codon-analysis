# -*- coding: utf-8 -*-

import pandas as pd

df_csv = pd.read_csv("bacteria_names_with_genus.txt", sep="\t", dtype={"partial": str, "GCA_Number": str})

df_csv = df_csv.groupby(['phylum', 'genus']).agg({'file_name':['count']})
df_csv.columns = df_csv.columns.droplevel(0)
df_csv.reset_index(inplace=True)

df_csv = df_csv.sort_values(['count'], ascending=False)

df_csv.to_csv('phylum_genus_occurrence_counts.txt', sep='\t')
print("Saved")