#!/usr/bin/env python
# coding: utf-8

import pandas as pd

df_csv = pd.read_csv("organism_names.txt", sep="\t", dtype={"partial": str, "GCA_Number": str})
print("Loaded")

domains = ["Bacteria"]
df_csv = df_csv[df_csv.domain.isin(domains)]
print("Filtered")

df_csv['gene'] = df_csv['gene'].str.lower()
print("Transformed")

## count per genus, populate genus and filter out "sp." organism names

df = df_csv[["gene", "start_codon", "domain", "kingdom", "phylum", "organism", "organism_name", "file_name", "start", "end", "strand", "phase"]]

genus = df["organism_name"].str.split(" ")

df["genus"] = [x[0] for x in genus]
df["species"] = [x[1] for x in genus]

specieses = ["sp."]
df = df[~df.species.isin(specieses)]

df_genus = df

df_genus.to_csv('bacteria_names_with_genus.txt', sep='\t')
print("Saved")